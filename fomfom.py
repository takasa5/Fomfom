"""Fom-fom by multilists"""
import subprocess
import zenhan

while True:
    CMD = "juman"
    PROC = subprocess.Popen(CMD, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    strin = input('>')
    PROC.communicate(strin.encode('cp932'))
    REP = PROC.communicate()
    line = REP[0].decode('cp932').split("\r\n")

    num = []
    num.append([])
    ope = []
    ope.append([])
    kara = -16
    to_cnt = 0
    for i in range(len(line)-2):
        if "@" not in line[i]:
            word = line[i].split()
            #print(line[i])
            if "数量" in word[11]:
                num[len(num)-1].append(zenhan.z2h(word[0]))
            if "たす" in word[11]:
                ope[len(ope)-1].append("+")
                if to_cnt >= 2:
                    for j in range(to_cnt-1):
                        ope[len(ope)-1].append("+")
                        to_cnt = 0
            if "ひく" in word[11]:
                ope[len(ope)-1].append("-")
                if kara > i - 2:
                    num.insert(0, num.pop(-2))
                    if len(num[0]) == 1:
                        ope.insert(0, ope.pop())
                    else:
                        ope.insert(0, ope.pop(-2))
            if ("ける" in word[11]) or ("かけ" in word[2]):
                ope[len(ope)-1].append("*")
            if "わる" in word[11]:
                ope[len(ope)-1].append("/")
            if "は" in word[2] and "助詞" in word[3]:
                num.append([])
                ope.append(['='])
            if "と" in word[2] and "助詞" in word[3]:
                if len(line) - i < 10:
                    num.append([])
                    ope.append(['='])
                else:
                    to_cnt += 1
                    ope[len(ope)-1].append('_')
            if "から" in word[2] and "格助詞" in word[5]:
                if not ope[len(ope)-1]:
                    ope[len(ope)-1].append('_')
                kara = i
            if (("で" in word[2] or "を" in word[2] or "から" in word[2]) and "助詞" in word[3]) or ("テ形" in word[9]):
                num.append([])
                ope.append([])
            if ("に" in word[2] and "助詞" in word[3]) or "それ" in word[2]:
                if not ope[len(ope)-1]:
                    ope[len(ope)-1].append('_')
                num.append([])
                ope.append([])

    split = [False] * len(num)
    for i, n in enumerate(num):
        if n: #無駄なブロック分かれを統合
            if not ope[i] or '_' in ope[i]:
                split[i] = True
                if split[i-1] is True: #次要素にて判断
                    while num[i]:
                        num[i-1].append(num[i].pop(0))

    for i, n in enumerate(num): #語順の演算子を数式順に移動
        if not n:
            if ope[i]:
                for j in range(i):
                    if '_' in ope[j]:
                        ope[j] = ope[i]
                        ope[i] = []
    for i, o in enumerate(ope):
        if '_' in o:
            ope[i] = []

    if not ope:
        print("演算を日本語で表してください(数字は全角入力)")
    else:
        for i, n in enumerate(num): #数が複数ある部分には対応する演算子を追加
            if len(n) > 1:
                for j in range(len(ope[i])):
                    num[i].insert(2 * j + 1, ope[i].pop(0))
        ope = [x for x in ope if x]

        for i, o in enumerate(ope): #足し算、引き算を追加
            if len(num[i]) == 1 and len(num[i+1]) == 1 and ('+' in o or '-' in o):
                num[i] = [num[i].pop(0), ope[i].pop(0), num[i+1].pop(0)]
        for i, n in enumerate(num): #複数ある要素に括弧を追加
            if len(n) > 1:
                num[i].insert(0, '(')
                num[i].append(')')

    ope = [x for x in ope if x]
    num = [x for x in num if x]

    #出力
    if not ope:
        for i, n in enumerate(num[0]):
            if n != '(' and n != ')':
                print(n, end=' ')
        print()
    else:
        for i, o in enumerate(ope):
            while num[i]:
                print(num[i].pop(0), end='')
            print('', ope[i][0], end=' ')
        while num[-1]:
            print(num[-1].pop(0), end='')
        print()
    PROC.kill()
