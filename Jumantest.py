﻿"""Fom-fom"""
import subprocess
import zenhan

while True:
    CMD = "juman"
    proc = subprocess.Popen(CMD, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    str = input('>')
    proc.communicate(str.encode('cp932'))
    repr = proc.communicate()
    line = repr[0].decode('cp932').split("\r\n")

    num = []
    ope = []
    kara = -16
    to_cnt = 0
    skip = False
    mul_div_status = False
    for i in range(len(line)-2):
        if "@" not in line[i]:
            word = line[i].split()
            #print(line[i])
            if "数量" in word[11]:
                num.append(zenhan.z2h(word[0]))
            if "たす" in word[11] or "プラス" in word[11]:
                ope.append("+")
                if to_cnt >= 2:
                    for i in range(to_cnt-1):
                        ope.append("+")
            if "ひく" in word[11] or "マイナス" in word[11]:
                ope.append("-")     #to_cntをつける
                if kara > i - 2:
                    num.insert(0, num.pop())
                    ope.insert(0, ope.pop())
            if ("ける" in word[11]) or ("かけ" in word[2]):
                ope.append("*")
            if "わる" in word[11]:
                ope.append("/")
            if ("は" in word[2]) and "助詞" in word[3]:
                ope.append("=")
            if "から" in word[2] and "格助詞" in word[5]:
                kara = i
            if "と" in word[2] and "助詞" in word[3]:
                to_cnt += 1
            if ("から" in word[2] and "接続助詞" in word[5]) or ("、" in word[2]) or ("テ形" in word[9]) or ("に" in word[2]and "助詞" in word[3]):    #試験的なカッコ処理 
                if len(line)-2 - i > 4:
                    for j in range(4): #解析文字列を後方にrange()だけ走査
                        tmpword = []
                        tmpword = line[i+j+1].split()
                        if ("ける" in tmpword[11]) or ("かけ" in tmpword[2]) or ("わる" in tmpword[11]): #×、÷が出てきたときフラグ
                            mul_div_status = True

                if len(ope) > 0 and mul_div_status is True:        #順番が重要でないときはかっこをつけないようにしたいが・・・→足し算、引き算の時はつけなくしたい
                    if ")" not in num[-1] and "(" not in num[-(len(ope)+1)]:
                        num[-1] = num[-1] + ")"
                        num[-(len(ope)+1)] = "(" + num[-(len(ope)+1)]
                        mul_div_status = False
            #ものを、それをが出てきたら2リスト管理？（「それ」は別の文脈でも出るので注意）
    if len(num) <= 1 or len(ope) == 0:
        print("演算を日本語で表してください(数字は全角入力)")
        skip = True
    else:
        i = 0
        open_bra = False
        for i in range(len(ope)):
            print(num[i], end=" ")
            print(ope[i], end=" ")
        if len(num) > i+2:
            print(num[i+1], end=" ")
            ope.append("=")
            print(ope[i+1], end=" ")
            print(num[i+2])
        else:
            print(num[i+1])
    proc.kill()
"""
#答え合わせパート
    if skip == True:
        pass
    elif "=" not in ope:
        print("不完全な式です")
    else:
        i = 0
        ans = int(num[0])
        for i in range(len(ope)-1):
            if ope[i] == "+":
                ans += int(num[i+1])
            elif ope[i] == "-":
                ans -= int(num[i+1])
            elif ope[i] == "*":
                ans *= int(num[i+1])
            elif ope[i] == "/":
                ans /= int(num[i+1])
        print("この式の正解:",int(ans))
        if ans != int(num[-1]):
            print("誤りを含む式です")
    proc.kill()
"""